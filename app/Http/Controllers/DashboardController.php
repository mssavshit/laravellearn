<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use view;

class DashboardController extends Controller
{
   
   public function __invoke()
   {
       dd("hello invoke");
   }

   public function __construct()
   {
     //  dd("call contrustor");
   }

   public function display()
   {
        $title = ['arrr'=>'page1', 'status'=>'pending'];
      return view('pages.dashboard',compact('title'));
   }

   public function nextpage()
   {
       return view('pages.user');
   }
}
