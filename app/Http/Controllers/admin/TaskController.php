<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Taskservice;

use App\Http\Requests\Newtask;

use view;

class TaskController extends Controller
{
    private $taskservice;
    private $taskrequest;

    public function __construct()
    {
        $this->taskservice = new Taskservice();
        $this->taskrequest = new Newtask();

    }

    public function index()
    {
         $data = $this->taskservice->firstdata();
        return view('pages.task.index',compact('data'));
    }

    public function listdata()
    {
        $data = $this->taskservice->listdata();
        return view('pages.task.list', compact('data'));
    }
    public function insertdata()
    {
        
       // $data= $request->all();

       // $message = $this->taskservice->insertsave($data);
        
        $data = $this->taskservice->firstdata();

        dd($data);

        return view('pages.task.index',compact('firstdata'));
    }

    public function deletedata()
    {

        $data= array( 
                [ "id"=>"1", "name"=> "Location 11", "status"=> "pending"],
                [ "id"=>"2", "name"=> "Location 22", "status"=> "pending"],
                [ "id"=>"3", "name"=> "Location 33", "status"=> "pending"]
                        
                    );
        
        $message = $this->taskservice->deletedata($data);
        
        echo json_encode($message);
    }


}
