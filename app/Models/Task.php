<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $table="tasks";
    protected $filable = ['name','status'];
    protected $dates = ['deleted_at'];
}
