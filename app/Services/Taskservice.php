<?php

namespace App\Services;
use App\Repository\TaskRepository;

class Taskservice 
{
    private $taskrespository;

    public function __construct($value='')
    {
        $this->taskrespository = new TaskRepository();
    }

    public function insertsave($data)
    {
        return $this->taskrespository->insertsave($data);
    }
    public function deletedata($data)
    {
        return $this->taskrespository->deletedata($data);
    }
    
    public function listdata()
    {
        return $this->taskrespository->listdata();
    }

    public function firstdata()
    {
            return $this->taskrespository->firstdata();
    }
}
