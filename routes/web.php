<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'project', 'uses'=>'DashboardController@display'] );
Route::get('/pri', ['as'=>'mahi','uses'=>'DashboardController@nextpage']);

Route::group(['namespace' => 'Admin'], function () {

    Route::get('/task-add', ['as'=>'task.add','uses'=>'TaskController@index']);

    Route::post('/task-data', ['as'=>'taskdataadd','uses'=>'TaskController@insertdata']);

    Route::get('/task-delete', ['as'=>'taskdatadelete','uses'=>'TaskController@deletedata']);
    
    Route::get('/task-list', ['as'=>'taskdatalist','uses'=>'TaskController@listdata']);
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
